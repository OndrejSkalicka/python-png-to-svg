# Python png to svg

Simple tool to convert PNG image to an SVG.

## Requirements

Python 3.6 + install requirements from `requirements.txt` using pip.

## Usage

`python png2svg.py myimage.png myvectors.svg`
